# Collaborative Data Analysis 
### Yilin Liu & Eva Wu

1. Design an analysis pipeline.

2. Build your pipeline using a shared repository. You will code together, using branching methods.

3. Incorporate modularity in your code. 

4. Explore the data, visually or otherwise.

5. Identify some interesting features or relationships.

6. Create effective visualization.

7. Present, as a team, your findings to the class in week 8.

8. During the presentation, you are also required to ask questions about another team's work.