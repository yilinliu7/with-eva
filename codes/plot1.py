import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def plot_dist(var):
  '''
  plot a histogram
  input: variable to be plotted
  output: a histogram plot
  '''
  sns.distplot(var, hist=True, kde=True, 
             bins=int(180/5), color = 'orange', 
             hist_kws={'edgecolor':'pink'},
             kde_kws={'linewidth': 1.5})

def plot_stacked_bar_by_gender(v_list, labels):
  '''
  plot a stacked bar chart w/ v_list on the x-axis & gender noted by color
  input: a list of columns to be plotted
  output: a stacked bar chart
  '''
  mean_male = df[df['SEX_female=1']==0][v_list].mean()
  mean_female = df[df['SEX_female=1']==1][v_list].mean()
  width = 0.35       # the width of the bars: can also be len(x) sequence

  fig, ax = plt.subplots()

  ax.bar(labels, mean_male, width, label='Male')
  ax.bar(labels, mean_female, width, bottom=mean_male, label='Female')

  ax.set_ylabel('Mean')
  ax.set_title('Mean by condition and gender')
  ax.legend()

  plt.show()