import pandas as pd

def load_data(fname):
    """
    Loads data from .csv file.
    input: fname - full path of file
    output: dataframe
    """
    df_loaded = pd.read_csv(fname)
    return df_loaded

def save_data(fname, df_in):
    """
    Saves data to .csv file.
    input: fname (full path of file) & df_in (dataframe to be saved)
    output: n/a
    """
    df_in.to_csv(fname)




