import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def plot_bar(x1, x2, color):
  '''
  plot a bar graph
  input: variables to be plotted
  output: a bar graph
  '''
  mean = df[[x1, x2]].mean()
  fig = plt.figure()
  ax = fig.add_axes([0,0,1,1])
  fairness = [x1, x2]
  response = [mean[0], mean[1]]
  ax.bar(fairness, response, color = color)
  plt.axhline(y=0.5, color='r', linestyle='-')
  plt.show()