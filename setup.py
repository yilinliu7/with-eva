import setuptools

setuptools.setup(
    name="codes",
    version="0.1.0",
    author="Eva & Yilin",
    author_email="wux21@uchicago.edu",
    description="Group Project for PSYC 40650",
    url="https://bitbucket.org/yilinliu7/with-eva/",
    packages=setuptools.find_packages(),
    install_requires=[
        'pandas',
        'matplotlib',
        'numpy',
        'scipy',
        'scikit-learn',
        'bokeh=2.2.2',
        'seaborn',
        'qgrid'
    ],
)